var app = {};
app.slide = document.getElementById("slide");
app.personName = document.getElementById("person-name");

function selectSlideItem(item){
    app.slide.scrollLeft = item.offsetLeft 
    - ((app.slide.getBoundingClientRect().width/2) 
    - (item.getBoundingClientRect().width/2) )
    - app.slide.getBoundingClientRect().left;

    app.personName.innerHTML = item.getAttribute("data-name");
}